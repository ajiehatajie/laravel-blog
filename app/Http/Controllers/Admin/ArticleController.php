<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Article;
use Illuminate\Support\Facades\Log;
use App\Model\Article as ArticleModel;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ArticleRequest;
use App\Model\Categories as CategoriesModel;
use App\Model\Tags;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('User showing Menu Article.', ['name' => Auth::user()->name]);
        $article=ArticleModel::orderBy('published_at','desc')->paginate(config('blog.PAGINATE'));
        return view('admin.article.index')->with('article',$article);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('User Will Created new Article.', ['name' => Auth::user()->name]);
        $category=CategoriesModel::pluck('name_categories','id');
        $tags=Tags::all();
       
        //return view('admin.article.create')->with('category',$category);
        return view('admin.article.create',compact('category','tags'));


       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ArticleRequest $request)
    {
       // dd($request->input('tags'));

        
        $db = new ArticleModel($request->all());
        $input=$request->all();
        $db->author_id=Auth::user()->id;
        $db->categories_id=$input['categories_id'];
        $db->hitview=0;
        $db->save();
        $db->tags()->attach($request->input('tags'));
       
        return redirect()->Route('article.index')->with('flash_message','Article Succesfully Save');
        
       
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ArticleRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
