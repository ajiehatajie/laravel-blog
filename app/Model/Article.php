<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
	protected $dates = ['published_at'];
  protected $table="articles";
  protected $fillable=['title','slug','content','author_id','categories_id','hitview','desc'];


  public function setTitleAttribute($value)
 	{
	    $this->attributes['title'] = $value;

	    if (! $this->exists) {
	      $this->attributes['slug'] = str_slug($value);
	    }
  	}


  	public function categoriesArticle()
  	{
  		return $this->belongsTo('App\Model\Categories','categories_id');
  	}

  	public function AuthorCreated()
  	{
  		return $this->belongsTo('App\User','author_id');
  	}

    public function tags()
    {
      return $this->belongsToMany('App\Model\PostTags','post_tags','article_id','tag_id');
          
    }
}
