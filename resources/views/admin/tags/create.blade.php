@extends('layouts.admin_layout')
@section('page','Edit Tags')

@section('content')
<div class="row">
        {!! Form::open(['route'=>'tags.store']) !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
    @endif

    <div class="form-group">
                               
        {!! Form::label('Name Tags') !!}
        {!! Form::text('name',null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        <p class="help-block">Example block-level help text here.</p>

    </div>
    
          

        {!! Form::submit(Lang::get('core.sb_save'),array('class'=>'btn btn-primary')) !!}

        {!! Form::close() !!}

    </div>
 	
</div>
@endsection

