<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Model\Article;
use Carbon\Carbon;
use App\Model\Categories;
use App\Model\Profile;
use Illuminate\Support\Facades\Log;

use App\Events\PostHitCounterEvent;

class BlogController extends Controller
{
    //
    //
    public $profil;


    public function __construct()
    {
        $this->profil=Profile::find('1');

    }

    public function index()
    {
        \VisitLog::save();
        //Log::info('User showing Menu Article.', ['name' => Auth::user()->name]);
        //
        
        return view('blog.index', compact('posts','categories'));
    }

      public function blog()
    {
       
       /* $posts = Article::where('published_at', '<=', Carbon::now())
            ->orderBy('published_at', 'desc')
            ->paginate('5');
        */
        \VisitLog::save();
        $posts = Article::orderBy('published_at','desc')->orderBy('published_at', 'desc')
            ->paginate(config('blog.PAGINATE'));
        $categories=Categories::all();
        return view('blog.blog', compact('posts','categories'));
    }

    public function showPost($slug)
    {
         \VisitLog::save();
        $post = Article::whereSlug($slug)->firstOrFail();
        event(new PostHitCounterEvent($slug));
        //return view('blog.post')->withPost($post,$profile);
        return view('blog.post',compact('post'));
           
    }

     public function  getLang($lang='en')
    {
        \Session::put('lang', $lang);
        #$request->session()->put('lang',  $lang);
        #\App::setLocale($lang);
        app()->setLocale(\Session::get('lang'));
        return  \Redirect::back();
        

    }

    public function ListCategories($slug)
    {
         \VisitLog::save();
        $categories =Categories::where('slug','=',$slug)->firstOrFail();
        
        return view('blog.categories', compact('categories'));
       
    }
}
