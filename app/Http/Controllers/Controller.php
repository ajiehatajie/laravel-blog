<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $level;

    public function __construct()
    {

        if (config('blog.MULTI_LANG') == 1)
        {

            $lang = (\Session::get('lang') != "" ? \Session::get('lang') : config('blog.LANG_DEFAULT') );
            \App::setLocale($lang);
        }

    }


}
