<?php

use App\Model\Profile;

class Site

{
	

	 public static function langOption()
    {
        $path = base_path().'/resources/lang/';
        $lang = scandir($path);

        $t = array();
        foreach($lang as $value) {
            if($value === '.' || $value === '..') {continue;}
            if(is_dir($path . $value))
            {
                $fp = file_get_contents($path . $value.'/info.json');
                $fp = json_decode($fp,true);
                $t[] =  $fp ;
            }

        }
        return $t;
    }

    public static function Profile()
    {
        $profile=Profile::find('1');

        return $profile;
    }
}