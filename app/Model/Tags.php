<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{

   protected $fillable=["name"];
   
   public function Article()
    {
    	return $this->BelongsToMany('App\Model\Article');
    }

}
