<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Sarfraznawaz2005\VisitLog\Models\VisitLog as VisitLogModel;

class VisitorController extends Controller
{
    //
    //
    public function index()
    {
    	$visitlogs = VisitLogModel::all();
        //return view('visitlog::index', compact('visitlogs'));
        return view('admin.visitor.visitor',compact('visitlogs'));
    }


    /**
     * Deletes a record.
     *
     * @param $id
     * @param VisitLogModel $visitLog
     * @return mixed
     */
    public function destroy($id, VisitLogModel $visitLog)
    {
        $visitLog = $visitLog->find($id);

        if ($visitLog && !$visitLog->delete()) {
            return Redirect::back()->withErrors($visitLog->errors());
        }

        return Redirect::back();
    }

    /**
     * Deletes all records.
     *
     * @param VisitLogModel $visitLog
     * @return mixed
     */
    public function destroyAll(VisitLogModel $visitLog)
    {
        if (!$visitLog->truncate()) {
            return Redirect::back()->withErrors($visitLog->errors());
        }

        return Redirect::back();
    }

}
