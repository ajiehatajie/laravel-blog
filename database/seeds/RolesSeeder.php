<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         DB::table('roles')->delete();
         DB::table('roles')->insert(array(
          	array('name'=>'Super Administrator','created_at'=>Carbon::now(),'updated_at'=>Carbon::now()),
          	array('name'=>'Member','created_at'=>Carbon::now(),'updated_at'=>Carbon::now()),

          	));

    }
}
