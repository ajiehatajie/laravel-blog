<?php

namespace App\Listeners;

use App\Events\PostHitCounterEvent as HitCounter;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use App\Model\Article;
use Illuminate\Http\Request;

use Illuminate\Database\Eloquent\Model;


class PostHitListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public $article;

    public function __construct(Article $article)
    {
        $this->article=$article;
    }

    /**
     * Handle the event.
     *
     * @param  HitCounter  $event
     * @return void
     */
    public function handle(HitCounter $event)
    {   

        #$article->increment('hitview');
        #$article->hitview += 1;
        Log::info('event PostHitListener.', ['slug' => $event->PostId]);
       
       
    }
}
