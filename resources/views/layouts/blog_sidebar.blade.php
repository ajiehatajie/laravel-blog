
                                <div class="col-lg-3 col-lg-push-1 col-md-2 right-rail">
<div class="rail-bio">
						<img src="/img/profile/{{Site::Profile()->image}}" alt="Matt Stauffer headshot" class="rail-bio__headshot">
						<p>
                            {!! str_replace('""', '', Site::Profile()->desc) !!}

                           
                        </p>

                        <p style="margin-top: 1.5em;"><a href="https://twitter.com/ajiehatajie" class="twitter-follow-button" data-show-count="true" data-size="medium" data-dnt="true">Follow @ajiehatajie</a>
                        <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script></p>
					</div>

                    <hr>

                    <div class="rail-book">
                        <p><strong>Like what you're reading?</strong></p>
                        <p>I wrote an entire 400+ page book for O'Reilly: <a href="http://shop.oreilly.com/product/0636920044116.do">Laravel: Up and Running</a>.</p>
                        <p>You can <a href="http://shop.oreilly.com/product/0636920044116.do">pre-order the ebook or print book today</a>.</p>
                        <a href="http://shop.oreilly.com/product/0636920044116.do"><img src="/assets/images/laravel-up-and-running-matt-stauffer.png" class="rail-book__image"></a>
                    </div>
                    <hr>

                    <div class="rail-email-signup">
                        <form action="https://tightenco.createsend.com/t/d/s/oujykd/" method="post" id="subForm" class="form-inline" accept-charset="utf-8">
                            <p style="font-weight: bold;">Subscribe to my newsletter for more content like this <em>and</em> special newsletter-only posts:</p>
                            <label for="email" class="sr-only">Email</label>
                            <input type="email" name="cm-oujykd-oujykd" id="email" class="form-control" placeholder="Your Email..." required>
                            <input type="submit" name="submit" id="submit" value="Sign me up!" class="btn btn-primary">
                        </form>
                    </div>
				</div>

