
@extends('layouts.masterblog')
@section('page','Dashboard')
@section('content')

<div class="col-md-10 col-lg-8">

    <link href="/css/comment.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/icon.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/comment.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/form.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/components/button.min.css" rel="stylesheet">
    <link href="{{ asset('/vendor/laravelLikeComment/css/style.css') }}" rel="stylesheet">

@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif
<script type="text/javascript">
  $(document).ready(function(){

    
    $("[data-toggle=tooltip]").tooltip();
});
</script>
  
    <h1>{{ $post->title }}</h1>
    
    <h5>{{ $post->published_at->format('M jS Y g:ia') }} | By <strong> {{$post->AuthorCreated->name}}</strong></h5>
    <hr>
        <div id="article">


              {!! $post->content !!}
    
         
        </div>
    <hr>

    <h5>Tags</h5>
    <ul>
      @foreach ($post->tags as $tag)
        <li>{{ $tag->name }}</li>
      @endforeach
    </ul>

    @include('laravelLikeComment::like', ['like_item_id' => 'image_31'])

    @include('laravelLikeComment::comment', ['comment_item_id' => $post->id])
     

    <button class="btn btn-primary" onclick="history.go(-1)">
      « Back
    </button>

</div>
@include('layouts.blog_sidebar')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="{{ asset('/vendor/laravelLikeComment/js/script.js') }}" type="text/javascript"></script>

@endsection