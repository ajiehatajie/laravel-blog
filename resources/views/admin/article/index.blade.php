@extends('layouts.admin_layout')
@section('page','Categories')
@section('content')


@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif

<a href="{{URL('admin/article/create')}}" class="btn btn-primary" >{{Lang::get('core.create')}}</a>
<br/>
<div class="row">
<table class="table" id="dataa"  class="stripe">
		<thead>
			<tr>
				<td>Title </td>
				<td>Content </td>
				<td>Categories </td>
				<td>Author </td>
				<td>{{ Lang::get('core.admin_menu_action')}}
				      
				</td>
				
			</tr>
		</thead>
		<tbody>
			 @foreach($article as $data)
					<tr>
						<td>{{ $data->title }}</td>
						<td>{{ str_limit($data->content,100) }}</td>
						<td>{{ $data->categoriesArticle->name_categories}}</td>
						<td>{{ $data->AuthorCreated->name}}</td>
						
						<td>
							 <a href="{{ route('article.show', $data->id) }}" class="btn btn-info">{{Lang::get('core.btn_view')}}</a>
                        	 <a href="{{ route('article.edit', $data->id) }}" class="btn btn-primary">{{Lang::get('core.btn_edit')}}</a>
                                
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['article.destroy', $data->id]
                                ]) !!}
                                {!! Form::submit(Lang::get('core.btn_remove'), ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
						</td>
					</tr>
			 @endforeach
		</tbody>
</table>
			{!! $article->links() !!}
</div>
@endsection

