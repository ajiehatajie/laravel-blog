
@extends('layouts.masterblog')
@section('page','categories')
@section('content')

<div class="col-md-10 col-lg-8">
@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif

  <article>
   <h1> {{$categories->name_categories}} </h1>
    <p> {{$categories->desc}} </p>
      <ol style="list-style: decimal inside;">
           @php $i=1;@endphp
            @foreach($categories->categoriesArticle as $article)
              <li> 
                    <a href="/blog/{{ $article->slug }}">{{ $article->title }} {{ $article->last}}</a>
              </li>
             
            @endforeach
      </ol>
 
 
</article>
</div>
@include('layouts.blog_sidebar')

@endsection
