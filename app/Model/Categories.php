<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //
    protected $table="categories";

    protected $fillable =['name_categories','slug','desc'];
   
  public function setTitleAttribute($value)
 	{
	    $this->attributes['name_categories'] = $value;

	    if (! $this->exists) {
	      $this->attributes['slug'] = str_slug($value);
	    }
  }

     public function categoriesArticle()
    {
      return $this->hasMany('App\Model\Article','categories_id');
    }
}
