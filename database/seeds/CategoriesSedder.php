<?php

use Illuminate\Database\Seeder;

class CategoriesSedder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       DB::table('categories')->delete();
       DB::table('categories')->insert(array(
          	array('name_categories'=>'PHP OOP'),
          	array('name_categories'=>'C#'),
          	array('name_categories'=>'Laravel'),

          	));



    }
}
