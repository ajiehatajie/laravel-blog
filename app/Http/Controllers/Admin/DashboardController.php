<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class DashboardController extends Controller
{
    //
    //
    //
    public function index()
    {
    	Log::info('User showing Menu dashboard.', ['name' => \Auth::user()->name]);
    	return view('admin.dashboard.index');
    }
}
