@extends('layouts.admin_layout')
@section('page','Visitor Logs')
@section('content')
 <!-- Bootstrap -->
  	
 <style>
        #table-log {
            font-size: 85%;
        }

        th, td {
            text-align: center;
        }
    </style>

        <div align="center">
            <h4><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Visit Log</h4>
        </div>

        <div class="col-sm-12 col-md-12 table-container table-responsive">

            @if (isset($errors) && count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops! Something went wrong!</strong>

                    <br><br>

                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <table cellspacing="0" width="100%" id="table-log"
                   class="table table-striped table-bordered table-hover table-condensed dt-responsive nowrap">
                @if (config('visitlog.iptolocation'))
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>IP</th>
                        <th>Browser</th>
                        <th>OS</th>
                        @if (config('visitlog.log_user'))
                            <th>User</th>
                        @endif
                        <th>Country</th>
                        <th>Region</th>
                        <th>City</th>
                        <th>Zip</th>
                        <th>Timezone</th>
                        <th>Lt, Ln</th>
                        <th>Updated</th>
                        @if (config('visitlog.delete_log_button'))
                            <th>Action</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($visitlogs as $key => $visitlog)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$visitlog->ip}}</td>
                            <td>{{$visitlog->browser}}</td>
                            <td>{{$visitlog->os}}</td>
                            @if (config('visitlog.log_user'))
                                <td>{{$visitlog->user_name}} ({{$visitlog->user_id}})</td>
                            @endif
                            <td>{{$visitlog->country_name}}</td>
                            <td>{{$visitlog->region_name}}</td>
                            <td>{{$visitlog->city}}</td>
                            <td>{{$visitlog->zip_code}}</td>
                            <td>{{$visitlog->time_zone}}</td>
                            <td>{{$visitlog->latitude}}, {{$visitlog->longitude}}</td>
                            <td title="{{$visitlog->updated_at}}">{{$visitlog->last_visit}}</td>
                            @if (config('visitlog.delete_log_button'))
                                <td align="center">
                                    <a title="Delete"
                                       class="confirm-delete text-danger"
                                       data-label="Visit Log"
                                       rel="{{route('__delete_visitlog__', ['id'=>$visitlog->id])}}"
                                       href="javascript:void(0);">
                                        <b class="glyphicon glyphicon-trash"></b>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                @else
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>IP</th>
                        <th>Browser</th>
                        <th>OS</th>
                        @if (config('visitlog.log_user'))
                            <th>User</th>
                        @endif
                        <th>Updated</th>
                        @if (config('visitlog.delete_log_button'))
                            <th>Action</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($visitlogs as $key => $visitlog)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$visitlog->ip}}</td>
                            <td>{{$visitlog->browser}}</td>
                            <td>{{$visitlog->os}}</td>
                            @if (config('visitlog.log_user'))
                                <td>{{$visitlog->user_name}} ({{$visitlog->user_id}})</td>
                            @endif
                            <td title="{{$visitlog->updated_at}}">{{$visitlog->last_visit}}</td>
                            @if (config('visitlog.delete_log_button'))
                                <td>
                                    <a title="Delete"
                                       class="confirm-delete text-danger"
                                       data-label="Visit Log"
                                       rel="{{route('__delete_visitlog__', ['id'=>$visitlog->id])}}"
                                       href="javascript:void(0);">
                                        <b class="glyphicon glyphicon-trash"></b>
                                    </a>
                                </td>
                            @endif
                        </tr>
                    @endforeach
                    </tbody>
                @endif
            </table>

            @if (config('visitlog.delete_all_logs_button') && $visitlogs->count())
                <div align="center">
                    <a title="Delete All Logs"
                       class="confirm-delete btn btn-danger"
                       data-label="All Visit Log"
                       rel="{{route('__delete_visitlog_all__')}}"
                       href="javascript:void(0);">
                        <b class="glyphicon glyphicon-trash"></b> Delete All Logs
                    </a>
                </div>
            @endif

        </div>
    </div>
</div>

<!-- delete confirm modal start -->
<div class="modal fade " id="modal-delete-confirm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header modal-header-danger">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span></button>
                <h4 class="modal-title">
                    <b class="glyphicon-4x glyphicon glyphicon-trash"></b>
                    Confirm Delete
                </h4>
            </div>

            <div class="modal-body"></div>

            <div class="modal-footer">
                <button class="btn btn-default col-sm-2 pull-right" data-dismiss="modal">
                    Close
                </button>

                <form action="#" method="POST" style="display: inline;">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}

                    <button style="margin-right: 10px;" type="button"
                            class="btn confirm-delete-red-button btn-danger col-sm-2 pull-right"
                            id="frm_submit">Delete
                    </button>
                </form>
            </div>

        </div>
    
<!-- delete confirm modal end -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->




@endsection