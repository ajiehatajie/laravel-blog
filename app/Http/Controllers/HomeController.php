<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
        #return view('admin.dashboard');
           
    }

    public function tes()
    {
        $value=config('blog.Default_Lang');

        return $value; 
     }


      public function  getLang($lang='en')
    {
        \Session::put('lang', $lang);
        #$request->session()->put('lang',  $lang);
        #\App::setLocale($lang);
        app()->setLocale(\Session::get('lang'));
        return  \Redirect::back();


    }
}
