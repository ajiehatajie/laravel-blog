@extends('layouts.app')
@section('page','New Article')
@section('content')
<div class="row">

    <script type="text/javascript">
         $(".tag").select2();
    </script>

        {!! Form::open(['route'=>'article.store']) !!}

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
               @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if(Session::has('flash_message'))
            <div class="alert alert-success">
                {{ Session::get('flash_message') }}
            </div>
    @endif

    <div class="form-group">
                               
        {!! Form::label('title') !!}
        {!! Form::text('title',null, array('placeholder' => 'Name','class' => 'form-control')) !!}
        

    </div>
       <div class="form-group">
             {!! Form::label('Categories') !!}<br/>
              {!! Form::select('categories_id',$category,array('class'=>'btn btn-primary')) !!}
            </div> 
        <div class="form-group">
             {!! Form::label('Tags') !!}<br/>
              <select name="tags[]" class="form-control tag" multiple="multiple">
                  @foreach ($tags as $tag)
                    <option value="{{ $tag->id}} ">{{$tag->name}}</option>
                  @endforeach
              </select>
        </div> 

     <div class="form-group">
                               
        {!! Form::label('desc') !!}
        {!! Form::textarea('desc',null, array('placeholder' => 'Name','class' => 'form-control','id'=>'desc-ckeditor')) !!}
        <p class="help-block">Example block-level help text here.</p>

    </div>
    <div class="form-group">
                               
        {!! Form::label('Content') !!}
        {!! Form::textarea('content',null, array('placeholder' => 'Name','class' => 'form-control','id'=>'article-ckeditor')) !!}
        <p class="help-block">Example block-level help text here.</p>

    </div>
                       
       
        {!! Form::submit(Lang::get('core.sb_save'),array('class'=>'btn btn-primary')) !!}

        {!! Form::close() !!}

    </div>
 	
</div>


@endsection
@section('scripts')

<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
        CKEDITOR.replace( 'article-ckeditor' );

</script>


@endsection
