


@extends('layouts.admin_layout')
@section('page','Edit Categories')

@section('content')
<div class="row">


                                {!! Form::model($detail, [
                        'method' => 'PATCH',
                        'route' => ['categories.update', $detail->id]
                                ]) !!}

                                 @if (count($errors) > 0)
                                <div class="alert bg-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(Session::has('flash_message'))
                                    <div class="alert alert-success">
                                        {{ Session::get('flash_message') }}
                                    </div>
                                @endif

                                  <div class="form-group">
                               
                                {!! Form::label('Nama Kategori') !!}
                                {!! Form::text('name_categories', $detail->name_categories, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                <p class="help-block">Example block-level help text here.</p>

                           </div>
                       

                           {!! Form::submit('Simpan',array('class'=>'btn btn-primary')) !!}

                                {!! Form::close() !!}

                         </div>
 	
</div>
@endsection

