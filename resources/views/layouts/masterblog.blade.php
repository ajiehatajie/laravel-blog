<!DOCTYPE html>
<html lang="{{Lang::get('core.flag')}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="/css/bootstrap-cosmo.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link rel="stylesheet" href="/prism.css">    
  
    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
  
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top "  role="navigation">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                        <li><a href="{{URL('/blog')}}" >Blog</a></li>
                    <li><a href="https://mattstauffer.co/talks" >Talks</a></li>
                    <li><a href="https://mattstauffer.co/podcasts" >Podcasts</a></li>
                    <li><a href="https://mattstauffer.co/newsletter" >Newsletter</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">{{Lang::get('blog.login')}}</a></li>
                            <li><a href="{{ url('/register') }}">{{Lang::get('blog.register')}}</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
        
                        @endif
                         <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Lang::get('blog.languange')}}

                                    <img class="flag-lang" 
                                    src="/img/{{Lang::get('core.flag')}}.png"> 
                                    
                                     <span class="caret"></span>
                                </a>
                            <ul class="dropdown-menu" role="menu">
                        @foreach(Site::langOption() as $lang)
                                <li>
                                <a href="{{ URL::to('blog/lang/'.$lang['folder'])}}"><img class="flag-lang" src="/img/{{$lang['folder']}}.png"> {{  $lang['name'] }}
                                </a>
                                </li>
                       @endforeach
                        </ul>
                         </li>
                      
                    </ul>


                   

                </div>
            </div>
        </nav>
        
        <div class="container">
                <div class="row">
                    
                    
                      @yield('content')
                  
                    
                </div>
            
        </div>
        
    </div>
    
        <footer class="footer">
            <div class="container">
                <p>
                &copy; 2016 {{config('app.name')}} &nbsp;&bull;&nbsp; <a href="http://twitter.com/ajiehatajie">@ {{config('app.name')}}</a> &nbsp;&bull;&nbsp; <a href="/blog/feed.atom">RSS</a> &nbsp;&bull;&nbsp; Like what I write? Hire <a href="http://benang.co.id/?utm_source=hatajie.com&utm_medium=footer">Bcomm</a> and we can work together
                </p>
        </div>
        </footer>
    <!-- Scripts -->
    <script src="/js/app.js"></script>
    <script src="/prism.js"></script>
    
</body>
</html>
