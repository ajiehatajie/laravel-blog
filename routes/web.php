<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
Route::get('/', function () {
    return view('welcome');
});

Route::get('/', function () {
    return redirect('/blog');
});



*/
use App\Model\Roles;
use App\User;
use App\Model\Article;

use Illuminate\Support\Facades\Auth;
use App\Model\Categories;


Route::get('/', 'BlogController@index');
Route::get('/blog', 'BlogController@blog');
Route::get('blog/{slug}', 'BlogController@showPost');
Route::get('blog/categories/{slug}', 'BlogController@ListCategories');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/tes', 'HomeController@tes');



Route::get('/blog/lang/{id}', 'BlogController@getLang');
 
             
Route::group(['namespace'=>'admin','middleware'=>'admin','group'=>'Admin'],
function()
{ 

  Route::get('/admin', array('as'=>'admin','uses'=>'DashboardController@index') );
  Route::resource('admin/article','ArticleController');
  Route::get('admin/categories/data',['as' =>'categories.data','uses'=>'CategoriesController@getdata']);
  Route::resource('admin/categories','CategoriesController');
  Route::resource('admin/tags','TagsController');
  
  Route::get('admin/profile/{id}', ['as'=>'profile','uses'=>'ProfileController@index']);
  Route::post('admin/profile', ['as'=>'profile.store','uses'=>'ProfileController@store']);
  
  //log
   Route::get('admin/visitorlog', ['as'=>'visitorlog','uses'=>'VisitorController@index']);

}
);

Route::get('/ok',function(){
	
  $session=\Session::get('lang');

  dd($session);
});

Route::get('level', function() {
    //

		$roles=Roles::all();

		foreach ($roles as $key )
		{
			echo $key->Useradmin;
		}

});

Route::get('leveladmin', function() {
    //

		$user=User::all();

		foreach ($user as $key )
		{
			echo $key->Roles;
		}

});

Route::get('who', function() {
   
  	$artikel=Article::all();

  	foreach ($artikel as $key )
  	{
  		//dd($key).'</br>';

  		dd($key->categoriesArticle);
  	}


});