@extends('layouts.admin_layout')
@section('page','Tags')
@section('content')


@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif

<a href="{{URL('admin/tags/create')}}" class="btn btn-primary" >{{Lang::get('core.create')}}</a>
<br/>
<div class="row">
<table class="table" id="dataa"  class="stripe">
		<thead>
			<tr>
				<td>{{ Lang::get('core.admin_categories_name') }} </td>
				<td>{{ Lang::get('core.admin_menu_action')}}
				      
				</td>
				
			</tr>
		</thead>
		<tbody>
			 @foreach($tags as $data)
					<tr>
						<td>{{$data->name}}</td>
						
						<td>
							 <a href="{{ route('categories.show', $data->id) }}" class="btn btn-info">{{Lang::get('core.btn_view')}}</a>
                        	 <a href="{{ route('categories.edit', $data->id) }}" class="btn btn-primary">{{Lang::get('core.btn_edit')}}</a>
                                
                                {!! Form::open([
                                    'method' => 'DELETE',
                                    'route' => ['categories.destroy', $data->id]
                                ]) !!}
                                {!! Form::submit(Lang::get('core.btn_remove'), ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
						</td>
					</tr>
			 @endforeach
		</tbody>
</table>
			{!! $tags->links() !!}
</div>
@endsection
@push('js')
<script type="text/javascript">
	$(function(){
		$("#data").DataTable({
			 processing: true,
		        serverSide: true,
		        ajax: '{{ url("admin/categories/data") }}',
		        columns: [
		            { data: 'name_categories', name: 'name_categories' },
		            { data: 'lastview', name: 'lastview' },
		            {data: 'action', name: 'action', orderable: false, searchable: false}
		        ]
		});
	});	
</script>

@endpush

