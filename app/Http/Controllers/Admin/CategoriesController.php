<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoriesRequest;
use App\Model\Categories;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;
use Datatables;
use DB;
class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $layout_admin;

    function __construct()
    {

    }

    public function index()
    {
        //
      $categories= Categories::orderBy('name_categories','asc')->paginate(config('blog.PAGINATE'));
      Log::info('User showing Menu Categories.', ['name' => Auth::user()->name]);
      return view('admin.categories.index')->with('categories',$categories);
    }

    public function getdata()
    {
       $users = Categories::select(['id', 'name_categories', 'lastview']);

        return Datatables::of($users)
            ->addColumn('action', function ($user)
            {
                return '<a href="categories/'.$user->id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                    <a href="categories/'.$user->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Delete</a>
                    
                    <form method="POST" url="'.route('categories.index').'/'.$user->id.'">
                        <input name="_method" type="hidden" value="'.csrf_token().'">
                        <button class="btn btn-danger">delete</button>
                    </form>
                  
                ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->removeColumn('password')
            ->make(true);

    }

     public function query()
    {
        $categories = Categories::all();
        

        return $this->applyScopes($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      Log::info('User Will Create New Categories.', ['name' => Auth::user()->name]);
      return view('admin.categories.create');
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriesRequest $request)
    {
       
        Log::info('User Saving New Categories.', ['name' => Auth::user()->name,'query'=>$request->all()]);
        $db = new Categories($request->all());
         $input=$request->all();
        $db->slug=\str_slug($input['name_categories']);
        $db->save();
        return redirect()->route('categories.index')->with('flash_message','Categories successful');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $detail=Categories::findOrfail($id);
         Log::info('User Show Categories.', ['name' => Auth::user()->name,'query'=>$id]);
         return view('admin.categories.show')->with('detail',$detail);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         Log::info('User will update Categories.', ['name' => Auth::user()->name,'query'=>$id]);
         $detail=Categories::findOrfail($id);
         return view('admin.categories.edit')->with('detail',$detail);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriesRequest $request, $id)
    {
        //
         Log::info('User update Categories.', ['name' => Auth::user()->name,'query'=>$request->all()]);

        $categories=Categories::findOrfail($id);

        $input = $request->all();

        $categories->fill($input)->save();
        

        //$request->session()->flash('flash_message', 'categories was successful!');
        return redirect()->route('categories.index')->with('flash_message','Categories successful Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::error('User Delete Categories', ['name' => Auth::user()->name,'query'=>$id]);
        $categories = Categories::findOrFail($id);
        $categories->delete();
        return redirect()->route('categories.index')->with('flash_message','categories deleted successfully');
    }
}
