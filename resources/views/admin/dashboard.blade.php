
@extends('layouts.admin_layout')
@section('page','Dashboard')

@section('content')
 {{ Lang::get('core.create') }}

 <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
 <a href="{{ url('/logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

     
    <script src="/js/app.js"></script>                                   
 @stop