<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Profile;
use App\Http\Requests\ProfileRequest;
use Image;
use Illuminate\Support\Facades\Auth;
use Crypt;

class ProfileController extends Controller
{
    //
    //
    public $id;

    public function __construct()
    {
    	$this->id=1;
    }


    public function index($id)
    {
    	$decrypt=Crypt::decrypt($id);

    	$profile=Profile::findOrFail($decrypt);
    	return view('admin.profile.index',compact('profile'));
    }

    public function store(ProfileRequest $request)
    {
    	$input=$request->all();
    	$image = $request->file('file_gambar');
        $input['file_gambar'] = uniqid().'.'.$image->getClientOriginalExtension(); # untuk penamaan file setelah upload
        $destinationPathThumbnail = public_path('/img/profile/mini');

        $img = Image::make($image->getRealPath());
        $img->resize(100, 100, function ($constraint)
        {
            $constraint->aspectRatio();
        }
        )->save($destinationPathThumbnail.'/'.$input['file_gambar']);

        $destinationPathOri = public_path('/img/profile');
        $image->move($destinationPathOri, $input['file_gambar']);

        $db = Profile::findOrfail("1");
        $db->user_id=Auth::user()->id;
        $db->image= $input['file_gambar'];
        $db->quotes= $input['quotes'];
        $db->desc= $input['desc'];
        $db->save();

        return back()
            ->with('flash_message','Image Upload successful')
            ->with('file_gambar',$input['file_gambar']);
    }

}
