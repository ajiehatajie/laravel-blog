
@extends('layouts.masterblog')
@section('page','Dashboard')
@section('content')

 <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1">
  
        @if(Session::has('flash_message'))
                        <div class="alert bg-success" role="alert"> 
                          <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                          <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                         </div>
         @endif

        <div class="homepage-body">
            
            <img src="/img/profile/{{Site::Profile()->image}}" alt="Matt Stauffer headshot" class="homepage-headshot">
         

            <p class="intro">
            
              {!! str_replace('""', '', Site::Profile()->desc) !!}
            
            </p>
        </div>
                       
  </div>
                   

                   

@endsection


