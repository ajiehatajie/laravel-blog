<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{config('blog.blog')}}</title>

<link href="/theme/lumino/css/datepicker3.css" rel="stylesheet">

<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.10/css/dataTables.bootstrap.min.css">

<link href="/theme/lumino/css/styles.css" rel="stylesheet">

<link href="/css/select2.css" rel="stylesheet">
<!--Icons-->
<script src="/theme/lumino/js/lumino.glyphs.js"></script>

<script  src="/js/select2.js"></script>
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<script src="js/respond.min.js"></script>
<![endif]-->
 <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><span>Lumino</span>Admin</a>
				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> User <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">

						

							<li><a href="{{URL('admin/profile')}}/{{\Crypt::encrypt(\Auth::user()->id)}}"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Profile</a></li>
							<li><a href="#"><svg class="glyph stroked gear"><use xlink:href="#stroked-gear"></use></svg> Settings</a></li>
							<li><a href="{{URL('/logout')}}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logout</a>
							

							 <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                             </form>
							</li>
						</ul>
					</li>
				</ul>

				<ul class="user-menu">
					<li class="dropdown pull-right">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked flag"><use xlink:href="#stroked-flag"></use></svg> Languange <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
						@foreach(Site::langOption() as $lang)
                                <li>
               					<a href="{{ URL::to('blog/lang/'.$lang['folder'])}}"><img class="flag-lang" src="/img/{{$lang['folder']}}.png"> {{  $lang['name'] }}
               					</a>
               					</li>
                       @endforeach
						</ul>
					</li>
				</ul>

			</div>
						
		</div><!-- /.container-fluid -->
	</nav>
		
		 @include('layouts.side_bar')
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home"><use xlink:href="#stroked-home"></use></svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">@yield('page')</h1>
			</div>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
					@yield('content')
					</div>
				</div>
			</div>
			
		</div><!--/.row-->
	</div>	<!--/.main-->




<script src="//code.jquery.com/jquery-1.12.3.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<script>
    $(document).ready(function () {
        var $body = $('body');

        $('#table-log').DataTable({
            "order": [0, 'asc'],
            "responsive": true,
            "pageLength": 10,
            "autoWidth": true,
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [-1]
                }
            ]
        });

        // confirm delete
        $body.on('click', '.confirm-delete', function (e) {
            var label = $(this).data('label');
            var $dialog = $('#modal-delete-confirm');

            $dialog.find('.modal-body').html('You are about to delete <strong>' + label + '</strong>, continue ?');
            $dialog.find('form').attr('action', this.rel);
            $dialog.modal('show');

            e.preventDefault();
        });

        $body.on('click', '.confirm-delete-red-button', function (e) {
            $(this).attr('disabled', true);
            $(this).closest('form')[0].submit();
        });
    });
</script>
<!-- App scripts -->
<!--
	<script src="/theme/lumino/js/jquery-1.11.1.min.js"></script>
	<script src="/theme/lumino/js/bootstrap.min.js"></script>
	<script src="/theme/lumino/js/chart.min.js"></script>
	<script src="/theme/lumino/js/chart-data.js"></script>
	<script src="/theme/lumino/js/easypiechart.js"></script>
	<script src="/theme/lumino/js/easypiechart-data.js"></script>
	<script src="/theme/lumino/js/bootstrap-datepicker.js"></script>
	<script src="/theme/lumino/js/bootstrap-table.js"></script>
-->


	<script>
		$('#calendar').datepicker({
		});

		!function ($) {
		    $(document).on("click","ul.nav li.parent > a > span.icon", function(){          
		        $(this).find('em:first').toggleClass("glyphicon-minus");      
		    }); 
		    $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
		}(window.jQuery);

		$(window).on('resize', function () {
		  if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
		})
		$(window).on('resize', function () {
		  if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
		})
	</script>	
	<!-- App scripts -->
	@stack('js')

</body>

</html>
