<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
               DB::table('articles')->delete();

         $faker = Faker::create();
      	foreach (range(1,100) as $index) 
      	{
       		$title = $faker->sentence;
			$slug = Str::slug($title);
       		 DB::table('articles')->insert([
            'title' => $title,
            'slug' => $slug,
            'desc' => $faker->word,
            'content' => $faker->paragraph,
            'author_id'=>'1',
            'categories_id' => '1',
            'hitview' => '1',            
            'created_at' => $faker->dateTime($max = 'now'),
            'updated_at' => $faker->dateTime($max = 'now'),
           'published_at' => $faker->dateTime($max = 'now'),
             
        ]);
      }

    }
}
