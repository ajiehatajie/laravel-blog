@extends('layouts.admin_layout')
@section('page','Profile')
@section('content')


@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif

<h1>Profile</h1>
 {!! Form::open(['route'=>'profile.store','files'=>'true']) !!}

  @if (count($errors) > 0)
       <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
         @endforeach
        </ul>
       </div>
  @endif

         @if(Session::has('flash_message'))
                                    <div class="alert alert-success">
                                        {{ Session::get('flash_message') }}
                                    </div>
          @endif

          <div class="form-group">
                               
          {!! Form::label('email') !!}
          {!! Form::text('email', $profile->email, array('placeholder' => 'email','class' => 'form-control')) !!}
                              
           </div>
                       
           <div class="form-group">
                                
            {!! Form::label('Browse Picture') !!}
            {!! Form::file('file_gambar', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
                                
                                
            </div>
            <div class="form-group">
                                
            {!! Form::label('desc') !!}
             {!! Form::textarea('desc',$profile->desc, array('placeholder' => 'desc','class' => 'form-control','id'=>'profile-ckeditor')) !!}
            </div>
              
             <div class="form-group">
                                
            {!! Form::label('Qoutes') !!}
             {!! Form::textarea('quotes',$profile->quotes, array('placeholder' => 'qoutes','class' => 'form-control','id'=>'qoutes-ckeditor')) !!}
            </div>


          {!! Form::submit('Simpan') !!}

          {!! Form::close() !!}

           </div>


<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<script>
        CKEDITOR.replace( 'profile-ckeditor' );
        CKEDITOR.replace( 'qoutes-ckeditor' );
        
</script>

@endsection

