<?php

namespace App\Listeners;

#use App\Events\Registered;
use Illuminate\Auth\Events\Registered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\RegisteredUserNotification as RegisteredNotify;
use App\User;

use Illuminate\Support\Facades\Log;

class UserNewRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
       $user =User::where('name','=',$event->user->name)->first();
       $event->user->notify(new RegisteredNotify($user));
       Log::info('New User Registered',['name'=>$event->user->name,'data'=>$event,'name'=>$event->user]);

      # dd($event->user->name);
    
    }
}
