
@extends('layouts.masterblog')
@section('page','Dashboard')
@section('content')
<div class="col-md-10 col-lg-8">
@if(Session::has('flash_message'))
                <div class="alert bg-success" role="alert"> 
                  <svg class="glyph stroked checkmark"><use xlink:href="#stroked-checkmark"></use></svg> {{ Session::get('flash_message') }}
                  <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                 </div>
 @endif

   <h1>{{Lang::get('blog.categories')}}</h1>
    <ul class="post-list">
      @foreach ($categories as $categori)
        <li class="post-list__post" style="margin-bottom: 0.3em;">
          <a href="/blog/categories/{{ $categori->slug }}" class="post-list__link">{{ $categori->name_categories }}</a>
         
        </li>
      @endforeach
    </ul>

    <h1>{{Lang::get('blog.article')}}</h1>
    <h5>Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}</h5>
    <hr>
    <ul class="post-list">
      @foreach ($posts as $post)
        <li>
          <a href="/blog/{{ $post->slug }}" class="post-list__link">{{ $post->title }}</a>
          <em>({{ $post->published_at->format('M jS Y g:ia') }})</em>
          <p  class="post-list__preview">

            {{ str_limit($post->desc,100) }} <br/>


          </p>
        </li>
      @endforeach
    </ul>
    <hr>
    {!! $posts->links() !!}


</div>
@include('layouts.blog_sidebar')


@endsection


